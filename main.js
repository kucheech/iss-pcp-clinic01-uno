const path = require('path');
const express = require('express');
const expressWS = require('express-ws');
const bodyParser = require('body-parser');

const UnoDeck = require('./libs/uno');
const uuid = require("uuid/v4");

const app = express();
const appWS = expressWS(app);

const games = {};

app.use(bodyParser.json());

app.post("/game", function (req, resp) {
	const gid = uuid().substring(0, 8);
	console.log(gid);
	games[gid] = {
		gamesId: gid,
		deck: null,
		table: null,
		players: []
	}

	resp.status(201).json({ gameId: gid });
});

app.ws("/game/:gid", function (ws, req) {
	const name = req.query.name;
	const gid = req.params.gid;

	// for table
	if ("TABLE" == name) {
		const game = games[gid];
		game.deck = new UnoDeck();
		game.deck.shuffle(20);
		for (var i in game.players) {
			var hand = game.deck.take(7);
			game.players[i].send(JSON.stringify(hand));
		}
		ws.on('message', function (message) {
			var cmd = JSON.parse(message);
			if ("drop" == cmd) {
				game.table.send(message);
			}
		})
		game.table = ws;
		const initalTable = {
			topCard: game.deck.take()[0],
			cardCount: game.deck.length
		}
		const cmd = { cmd: "init", setup: initalTable };
		game.table.send(cmd);
		return;
	}

	// for Player
	ws.playerName = name;
	games[gid].players.push(ws);
	ws.on('message', function (message) {

	})
})

app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "bower_components")));

const port = process.env.APP_PORT || 3000;
app.listen(port, function () {
	console.info("Application started on port %d at %s"
		, port, new Date());
});
