(function () {
  'use strict';

  angular
    .module('TableApp')
    .controller('TableCtrl', TableCtrl);

  TableCtrl.$inject = ["$http", "$scope"];
  function TableCtrl($http, $scope) {
    var vm = this;
    vm.gameId = "Not started yet";
    vm.createGame = createGame;
    vm.startGame = startGame;
    vm.gameCreated = false;
    vm.topCard = null;
    vm.cardCount = 0;

    init();

    ////////////////

    function startGame() {
      vm.socket = new WebSocket("ws://localhost:3000/game/" + vm.gameId + "?name=TABLE");
      vm.socket.onmessage = onmessage;
    }

    function onmessage(event) {
      $scope.$apply(function(){
        const data = event.data;
        const cmd = data.cmd;
        if(cmd == "init") {
          vm.topCard = data.topCard;
          vm.cardCount = data.cardCount;
        } else {
          
        }
        
        
      });
    }


    function createGame() {
      $http.post("/game").then(function (result) {
        vm.gameId = result.data.gameId;
        vm.gameCreated = true;
      });
    }

    function init() {
      vm.gameCreated = false;
      vm.topCard = null;
      vm.cardCount = 0;
    }
  }
})();