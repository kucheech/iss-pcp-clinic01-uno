(function () {
  'use strict';

  angular
    .module('PlayerApp')
    .controller('PlayerCtrl', PlayerCtrl);

  PlayerCtrl.$inject = ['$http', "$scope"];
  function PlayerCtrl($http, $scope) {
    var vm = this;
    vm.name = "";
    vm.gameId = "";
    vm.hand = [];
    vm.socket = null;
    vm.join = join;
    vm.dropCard = dropCard;

    init();

    ////////////////

    function dropCard(index) {
      var toDrop = vm.hand.splice(index,1);
    }

    function join() {
      console.log("join")
      vm.socket = new WebSocket("ws://localhost:3000/game/" + vm.gameId + "?name=" + vm.name);
      vm.socket.onmessage = onmessage;
    }

    function onmessage(event) {
      $scope.$apply(function () {
        vm.hand = JSON.parse(event.data)
        console.log(vm.hand);
      })
    }

    function init() { }
  }
})();